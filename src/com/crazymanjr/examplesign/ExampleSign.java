package com.crazymanjr.examplesign;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.crazymanjr.examplesign.listeners.Listeners;
import com.crazymanjr.examplesign.manager.PacketSignManager;

public class ExampleSign extends JavaPlugin 
{

	public void onEnable() 
	{
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
		PacketSignManager.get().load();
	}

}

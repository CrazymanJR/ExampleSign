package com.crazymanjr.examplesign.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.block.Sign;

public class PacketSign
{
	
	private Sign _sign;
	private String[] _lines;
	private List<String> _hidden = new ArrayList<String>();
	
	
	public PacketSign(Sign sign, String[] lines)
	{
		setSign(sign);
		setLines(lines);
		
		for (int i = 0; i < 3; i++) 
		{
			sign.setLine(i, lines[i]);
		}
		
	}

	public Sign getSign() 
	{
		return _sign;
	}

	public void setSign(Sign sign) 
	{
		this._sign = sign;
	}

	public String[] getLines() 
	{
		return _lines;
	}

	public void setLines(String[] lines) 
	{
		this._lines = lines;
	}

	public List<String> getHidden() {
		return _hidden;
	}

}

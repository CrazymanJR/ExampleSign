package com.crazymanjr.examplesign.listeners;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutUpdateSign;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.crazymanjr.examplesign.manager.PacketSignManager;
import com.crazymanjr.examplesign.objects.PacketSign;

public class Listeners implements Listener 
{

	@EventHandler
	public void onCreate(SignChangeEvent event) 
	{
		Sign sign = (Sign) event.getBlock().getState();
		
		if (!PacketSignManager.get().getSigns().contains(sign)) 
		{
			PacketSignManager.get().add(new PacketSign(sign, event.getLines()));
		}
		
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) 
	{
		if (event.getBlock().getState() instanceof Sign) 
		{
			
			Sign sign = (Sign) event.getBlock().getState();
		
			if (PacketSignManager.get().getSigns().contains(sign)) 
			{
				PacketSignManager.get().remove(PacketSignManager.get().getSign(sign));
			}
		
		}
		
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) 
	{
		Player player = event.getPlayer();
		
		for (Sign s : PacketSignManager.get().getSigns()) 
		{
			
			PacketSign packetSign = PacketSignManager.get().getSign(s);
			CraftWorld world = (CraftWorld) s.getLocation().getWorld();
			BlockPosition blockPosition = new BlockPosition(s.getX(), s.getY(), s.getZ());
           
			if (signActive(s)) 
			{
			
				if (player.getLocation().distance(s.getLocation()) >= 4 && !packetSign.getHidden().contains(player.getName())) 
				{
				
					packetSign.getHidden().add(player.getName());
				
					IChatBaseComponent[] text = {ChatSerializer.a("{\"text\":\"" + "" + "\"}"),
												ChatSerializer.a("{\"text\":\"" + "" + "\"}"),
												ChatSerializer.a("{\"text\":\"" + "" + "\"}"),
												ChatSerializer.a("{\"text\":\"" + "" + "\"}")};
				
					PacketPlayOutUpdateSign packet = new PacketPlayOutUpdateSign(world.getHandle(), blockPosition, text);
					((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
				
				} 
				else if (player.getLocation().distance(s.getLocation()) < 4 && packetSign.getHidden().contains(player.getName()))
				{
				
					packetSign.getHidden().remove(player.getName());

					IChatBaseComponent[] text = {ChatSerializer.a("{\"text\":\"" + s.getLine(0) + "\"}"),
												ChatSerializer.a("{\"text\":\"" + s.getLine(1) + "\"}"),
												ChatSerializer.a("{\"text\":\"" + s.getLine(2) + "\"}"),
												ChatSerializer.a("{\"text\":\"" + s.getLine(3) + "\"}")};
				
					PacketPlayOutUpdateSign packet = new PacketPlayOutUpdateSign(world.getHandle(), blockPosition, text);
					((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
				
				}
			
			} 
			else 
			{
				PacketSignManager.get().remove(packetSign);
			}
	
		}
	}
	
	private boolean signActive(Sign s) 
	{
		
		/* 
		Using 'getBlocksAround' because the exact location of the sign isn't the exact location, 
		so I check around. 
		*/
		
		List<Location> location = getBlocksAround(s.getLocation(), 2);
		
		for (int i = 0; i < location.size(); i++) 
		{
			Block b = s.getWorld().getBlockAt(location.get(i));
			if (b.getState() instanceof Sign) 
			{
				Sign sign = (Sign) b.getState();
				
				if (sign.equals(s)) 
				{
					return true;
				}
			}
		}
		
		return false;
		
	}
	
	private List<Location> getBlocksAround(Location l, int radius) 
	{
		World w = l.getWorld();
		int xCoord = (int) l.getX();
		int zCoord = (int) l.getZ();
		int YCoord = (int) l.getY();

		List<Location> tempList = new ArrayList<Location>();
		for (int x = -radius; x <= radius; x++) 
		{
			for (int z = -radius; z <= radius; z++) 
			{
				for (int y = -radius; y <= radius; y++) 
				{
					tempList.add(new Location(w, xCoord + x, YCoord + y, zCoord+ z));
				}
			}
		}
		return tempList;
	}
	
}

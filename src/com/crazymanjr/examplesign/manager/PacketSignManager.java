package com.crazymanjr.examplesign.manager;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

import com.crazymanjr.examplesign.objects.PacketSign;

public class PacketSignManager {
	
	private static PacketSignManager _instance;

	public static PacketSignManager get() 
	{
		if (_instance == null) 
		{
			_instance = new PacketSignManager();
		}
		
		return _instance;
	}
	
	private List<PacketSign> _signs = new ArrayList<PacketSign>();
	
	public void add(PacketSign sign) 
	{
		if (!getPacketSigns().contains(sign))
			getPacketSigns().add(sign);
	}
	
	public void remove(PacketSign sign) 
	{
		if (getPacketSigns().contains(sign))
			getPacketSigns().remove(sign);
	}
	
	public PacketSign getSign(PacketSign sign) 
	{
		if (getPacketSigns().contains(sign)) {
			return sign;
		}
		
		return null;
	}
	
	public PacketSign getSign(Sign sign) 
	{
		for (PacketSign s : getPacketSigns()) {
			if (s.getSign().equals(sign)) {
				return s;
			}
		}
		
		return null;
	}
	
	public List<PacketSign> getPacketSigns() 
	{
		return _signs;
	}
	
	public List<Sign> getSigns()
	{
		List<Sign> signList = new ArrayList<Sign>();
		
		for (PacketSign s : _signs)
		{
			signList.add(s.getSign());
		}
		
		return signList;
	}
	
	public void load() 
	{
		
		getPacketSigns().clear();
		
		for (World w : Bukkit.getWorlds()) 
		{
			for (Chunk c : w.getLoadedChunks()) 
			{
				for (BlockState b : c.getTileEntities()) 
				{
					if (b instanceof Sign) 
					{
						Sign s = (Sign) b;
						if (!getPacketSigns().contains(getSign(s))) 
						{
							add(new PacketSign(s, s.getLines()));
						}
					}
				}
			}
		}
	}

}
